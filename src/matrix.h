/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#ifndef __matrix_h_42378932389463762378992__
#define __matrix_h_42378932389463762378992__

#include <string>
#include "./smcalc.h"

using std::vector;
using std::string;

class Matrix {
  private:
    matrix_t matrix;
    string   name;
    UI       rows;
    UI       cols;

    double det_formula_2() const;
    double det_formula_3() const;
    /* converts this->matrix from vec<vec<int>> to vec<vec<double>> */
    dmatrix_t convert_to_double() const;
    double det_GEM() const;

  public:
    Matrix(const string &name, const matrix_t &matrix);
    string get_name() const;
    UI get_rows() const;
    UI get_cols() const;
    bool is_square() const;
    bool is_empty() const;
    matrix_t get_matrix() const;
    double get_determinant();
    void set_name(const string name);
    void transpone();
    Matrix add(const Matrix *secondmatrix, const string *resultname) const;
    Matrix multiply(const Matrix *secondmatrix, const string *resultname) const;
    void multiply_c(const int *scalar);

    vector<int> operator[](const UI &rowID) const;
};

#endif  // __matrix_h_42378932389463762378992__

/* EOF */
