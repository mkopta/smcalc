/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <cassert>
#include "./smcalc.h"
#include "./matrix.h"
#include "./common.h"

Matrix::Matrix(const string & _name, const matrix_t & _matrix)
  :matrix(_matrix), name(_name) {
  this->rows = _matrix.size();
  if (this->rows == 0) {
    this->cols = 0;
  } else {
    this->cols = _matrix[0].size();
  }
}

matrix_t Matrix::get_matrix() const {
  return(this->matrix);
}

string Matrix::get_name() const {
  return(this->name);
}

UI Matrix::get_rows() const {
  return(this->rows);
}

UI Matrix::get_cols() const {
  return(this->cols);
}

void Matrix::set_name(const string _name) {
  this->name = _name;
}

bool Matrix::is_square() const {
  return(this->cols == this->rows);
}

bool Matrix::is_empty() const {
  return(((this->cols == 0) || (this->rows == 0))?true:false);
}

double Matrix::det_formula_2() const {
  double det = 0.0f;
  // Well, this is obvious
  // http://en.wikipedia.org/wiki/Determinant#2-by-2_matrices
  det += this->matrix[0][0] * this->matrix[1][1];
  det -= this->matrix[0][1] * this->matrix[1][0];
  return(det);
}

double Matrix::det_formula_3() const {
  double det = 0.0f;
  // RULE OF SARRUS
  // http://en.wikipedia.org/wiki/Rule_of_Sarrus
  det += this->matrix[0][0] * this->matrix[1][1] * this->matrix[2][2];
  det += this->matrix[0][1] * this->matrix[1][2] * this->matrix[2][0];
  det += this->matrix[0][2] * this->matrix[1][0] * this->matrix[2][1];
  det -= this->matrix[0][2] * this->matrix[1][1] * this->matrix[2][0];
  det -= this->matrix[0][1] * this->matrix[1][0] * this->matrix[2][2];
  det -= this->matrix[0][0] * this->matrix[1][2] * this->matrix[2][1];
  return(det);
}

dmatrix_t Matrix::convert_to_double() const {
  dmatrix_t newmatrix;
  for (UI i = 0; i < this->cols; i++) {
    vector<double> line;
    for (UI j = 0; j < this->rows; j++) {
      line.push_back(this->matrix[i][j]);
    }
    newmatrix.push_back(line);
  }
  return(newmatrix);
}

double Matrix::det_GEM() const {
  double det = 1.0f;
  dmatrix_t matrix = convert_to_double();
  int n = matrix.size();  // matrix is square, n = rows & n = cols
  for (int x = 1; x < n; x++) {
    for (int line = x; line < n; line++) {
      double second = matrix[line][x-1];
      for (int pos = 0; pos < n; pos++) {
        int counter = 0;
        if (matrix[x-1][x-1] == 0) {
          // Zero pivot, cannot divide by zero
          for (int find = x; find < n; find++) {
            if (matrix[find][x-1] != 0) matrix[find].swap(matrix[x-1]);
          }
          if (counter == 0) return(0.0f);
        }
        if (second == 0 ) break;
        matrix[line][pos] -= (matrix[x-1][pos] * second) / matrix[x-1][x-1];
      }
    }
  }
  // we have row echelon form, multipliyng diagonal
  for (int i = 0; i < n; i++) det *= matrix[i][i];
  return(det);
}

double Matrix::get_determinant() {
  assert(this->is_square());
  double det = 0.0f;
  if (this->rows == 1) {
    det = this->matrix[0][0];
  } else if (this->rows == 2) {
    det = det_formula_2();
  } else if (this->rows == 3) {
    det = det_formula_3();
  } else {
    det = det_GEM();  // determinant by gauss elemination method
  }
  return(det);
}

void Matrix::transpone(void) {
  matrix_t transponed;
  if (this->rows == 0) return;  // nothing to transpone
  for (unsigned int i = 0; i < this->cols; i++) {
    vector<int> line;
    for (unsigned int j = 0; j < this->rows; j++)
      line.push_back(this->matrix[j][i]);
    transponed.push_back(line);
  }

  int tmp      = this->rows;
  this->rows   = this->cols;
  this->cols   = tmp;
  this->matrix = transponed;
}

/* Sums this matrix with second matrix and return result matrix */
Matrix Matrix::add(const Matrix *secondmatrix, const string *resultname) const {
  assert(this->cols == secondmatrix->get_cols());
  assert(this->rows == secondmatrix->get_rows());
  matrix_t result;
  for (UI i = 0; i < this->rows; i++) {
    vector<int> line;
    for (UI j = 0; j < this->cols; j++)
      line.push_back(this->matrix[i][j] + (*secondmatrix)[i][j]);
    result.push_back(line);
  }
  return(Matrix(*resultname, result));
}

/* Multiply this matrix with given matrix and return result matrix */
Matrix Matrix::multiply(const Matrix *secondmatrix, const string *resultname)
  const {
  matrix_t result;
  assert(this->cols == secondmatrix->get_rows());
  for (UI i = 0; i < this->rows; i++) {
    vector<int> line;
    for (UI j = 0; j < secondmatrix->get_cols(); j++) {
      int n = 0;
      for (UI k = 0; k < this->cols; k++) {
        n += this->matrix[i][k] * (*secondmatrix)[k][j];
      }
      line.push_back(n);
    }
    result.push_back(line);
  }
  return(Matrix(*resultname, result));
}

/* Multiply this matrix with given constant */
void Matrix::multiply_c(const int *scalar) {
  for (UI i = 0; i < this->rows; i++)
    for (UI j = 0; j < this->cols; j++)
      this->matrix[i][j] *= *scalar;
}

vector<int> Matrix::operator[](const UI & rowID) const {
  assert(rowID < this->rows);
  return(this->matrix[rowID]);
}

/* EOF */
