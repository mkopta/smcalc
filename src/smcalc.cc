/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <string>
#include <sstream>
#include "./smcalc.h"
#include "./matrix.h"
#include "./common.h"
#include "./import.h"
#include "./export.h"

/* maximum command length */
#define kCmdSize 256

using std::string;
using std::vector;

/* global variables */
vector<string> cmdargs;
vector<Matrix> matrices;

/* prototypes */
Matrix create_demo_matrix();
UI find_matrix(const string &mname);
bool process(const string &cmd);
int  ask_for_number(const string question);
int argc();
int rnd(int min, int max);
string ask_for_string(const string question);
vector<string> explode(const string &pattern, const string &text);
void adding();
void copy();
void create();
void det();
void fill();
void fill_from(UI mID);
void fill_hand(UI mID);
void fill_random(UI mID);
void help();
void info();
void list();
void list_matrices();
void multiply();
void print();
void remove();
void rename();
void transpone();

/* ********************************************************************* */

int argc() {
  return(static_cast<int>(cmdargs.size()));
}

/* Displays given question and return the number */
int ask_for_number(const string question) {
  char answer[20];  // FIXME(dum8d0g) - is 20 enough?
  int number;
  printf("%s: ", question.c_str());
  fflush(stdout);
  fgets(answer, 20, stdin);
  number = atoi(answer);
  return(number);
}

/* Displays given question and return the answer */
string ask_for_string(const string question) {
  int i = 0;
  string answer_str;
  char answer[100];  // FIXME(dum8d0g) - may magical constant disapear

  printf("%s: ", question.c_str());
  fflush(stdout);
  fgets(answer, 100, stdin);
  while (*(answer + i) != '\n' && *(answer + i) != '\0') {
    answer_str.push_back(*(answer + i));
    i++;
  }
  return(answer_str);
}

/* Creates demo matrix with random size and content */
Matrix create_demo_matrix() {
  // TODO(dum8d0g) - rnd? isn't better predefined values for demo?
  vector<        int  > v;
  vector< vector<int> > vv;
  const UI dim =(UI) rnd(2, 6);
  for (UI i = 0; i < dim ; i ++) {
    for (UI j = 0; j < dim ; j ++) {
      v.push_back(rnd(0, 9));
    }
    vv.push_back(v);
    v.clear();
  }
  Matrix demo("demo", vv);
  return(demo);
}

/* For given name finds an ID of matrix in list of matrices */
UI find_matrix(const string &mname) {
  for (UI i = 0; i < matrices.size(); i++)
    if (matrices[i].get_name() == mname)
      return(i);
  return(matrices.size());  // not found
}

/* Prints help in program */
void help() {
  printf(" exit  - exit the smcalc\n");
  printf(" quit  - quit the smcalc\n");
  printf(" q     - quit the smcalc\n");
  printf(" help  - print this help\n");
  printf(" ls    - list all matrices\n");
  printf(" list  - list all matrices\n");
  printf(" load  - load matrix from file\n");
  printf(" save  - save matrix to file\n");
  printf(" print - print a matrix\n");
  printf(" p     - print a matrix\n");
  printf(" new   - creates a new matrix\n");
  printf(" ren   - renames a matrix\n");
  printf(" mv    - renames a matrix\n");
  printf(" cp    - copy a matrix\n");
  printf(" rm    - remove a matrix\n");
  printf(" fill  - fill a matrix\n");
  printf(" tran  - transpone a matrix\n");
  printf(" info  - prints info about matrix\n");
  printf(" +     - add matrix to matrix\n");
  printf(" add   - add matrix to matrix\n");
  printf(" *     - multiply matrix by matrix\n");
  printf(" mul   - multiply matrix by matrix\n");
  printf(" mulc  - multiply matrix by scalar\n");
  printf(" det   - calculate determinant of matrix\n");
}

/* List existing matrices (name and size) */
void list() {
  if (argc() != 1) {
    fprintf(stderr, "No argument needed. Usage: 'ls'\n");
    return;
  }
  for (UI i = 0; i < matrices.size(); i++) {
    printf("%10s (%dx%d)\n", matrices[i].get_name().c_str(),
        matrices[i].get_rows(), matrices[i].get_cols());
  }
}

/* Print matrix on screen */
void print() {
  if (argc() != 2) {
    fprintf(stderr, "One argument needed. Usage: 'p[rint] <matrixname>'\n");
    return;
  }
  UI mID;
  string name = cmdargs[1];
  if ((mID = find_matrix(name)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", name.c_str());
    return;
  }
  UI rows = matrices[mID].get_rows();
  UI cols = matrices[mID].get_cols();
  for (UI i = 0; i < rows ; i++) {
    for (UI j = 0; j < cols; j++) {
      printf("%4i ", matrices[mID][i][j]);  // FIXME(dum8d0g):is 4 enough?
    }
    printf("\n");
  }
}

/* Create a new matrix */
void create() {
  if (argc() != 2) {
    fprintf(stderr, "One argument needed. Usage: 'create <matrixname>'\n");
    return;
  }
  string name = cmdargs[1];
  UI mID;
  if ((mID = find_matrix(name)) != matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" already exists.\n", name.c_str());
    return;
  }
  int rows = ask_for_number("Rows");
  int cols = ask_for_number("Cols");
  matrix_t mat;
  for (int i = 0; i < rows; i++) {
    vector<int> line;
    for (int j = 0; j < cols; j++)
      line.push_back(0);
    mat.push_back(line);
  }
  Matrix m(name, mat);
  matrices.push_back(m);
}

/* Copy matrix */
void copy() {
  UI mID;
  if (argc() != 3) {
    printf("Two arguments needed. Usage: 'cp <orig> <copy>'\n");
  } else if ((mID = find_matrix(cmdargs[1])) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found\n", cmdargs[1].c_str());
  } else if (find_matrix(cmdargs[2]) != matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" already exists.\n", cmdargs[2].c_str());
  } else {
    matrices.push_back(Matrix(cmdargs[2], matrices[mID].get_matrix()));
  }
}

/* Renames a matrix */
void rename() {
  UI mID;
  if (argc() != 3) {
    printf("Two arguments needed. Usage: 'ren <matrix> <newmatrix>'\n");
  } else if ((mID = find_matrix(cmdargs[1])) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found\n", cmdargs[1].c_str());
  } else if (find_matrix(cmdargs[2]) != matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" already exists.\n", cmdargs[2].c_str());
  } else {
    /*
    matrices.push_back(Matrix(cmdargs[2], matrices[mID].get_matrix()));
    matrices.erase(matrices.begin() + mID);
    */
    matrices[mID].set_name(cmdargs[2]);
  }
}

/* Removes matrix from list of matrices */
void remove() {
  if (argc() != 2) {
    printf("One argument needed. Usage: 'rm <matrixname>'\n");
    return;
  }
  string name = cmdargs[1];
  UI mID;
  if ((mID = find_matrix(name)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found\n", name.c_str());
    return;
  }
  matrices.erase(matrices.begin() + mID);
}

/* Fill given matrix by random numbers */
void fill_random(const UI mID) {
  int min = ask_for_number("Minimum");
  int max = ask_for_number("Maximum");
  UI m_i = matrices[mID].get_rows();
  UI m_j = matrices[mID].get_cols();
  matrix_t m_new;
  for (UI i = 0; i < m_i; i++) {
    vector<int> v;
    for (UI j = 0; j < m_j; j++)
      v.push_back(rnd(min, max));
    m_new.push_back(v);
  }
  Matrix mm(matrices[mID].get_name(), m_new);
  matrices[mID] = mm;
}

/* Fill matrix manualy (number by number) */
void fill_hand(UI mID) {
  UI m_i = matrices[mID].get_rows();
  UI m_j = matrices[mID].get_cols();
  matrix_t m_new;
  for (UI i = 0; i < m_i; i++) {
    vector<int> v;
    for (UI j = 0; j < m_j; j++) {
      std::stringstream ss;
      ss << "(" << i << ", " << j << ")";
      v.push_back(ask_for_number(ss.str()));
    }
    m_new.push_back(v);
  }
  Matrix mm(matrices[mID].get_name(), m_new);
  matrices[mID] = mm;
}

/* Fills from number to number+(m*n) */
void fill_from(UI mID) {
  int min = ask_for_number("From");
  UI m_i = matrices[mID].get_rows();
  UI m_j = matrices[mID].get_cols();
  matrix_t m_new;
  for (UI i = 0; i < m_i; i++) {
    vector<int> v;
    for (UI j = 0; j < m_j; j++)
      v.push_back(min++);
    m_new.push_back(v);
  }
  Matrix mm(matrices[mID].get_name(), m_new);
  matrices[mID] = mm;
}

/* Create identy matrix */
void fill_ident(UI mID) {
  if (matrices[mID].get_rows() != matrices[mID].get_cols()) {
    fprintf(stderr, "Matrix \"%s\" is not square.\n",
        matrices[mID].get_name().c_str());
    return;
  }
  matrix_t m_new;
  for (UI i = 0; i < matrices[mID].get_rows(); i++) {
    vector<int> line;
    for (UI j = 0; j < matrices[mID].get_cols(); j++) {
      if (i == j) {
        line.push_back(1);
      } else {
        line.push_back(0);
      }
    }
    m_new.push_back(line);
  }
  Matrix mm(matrices[mID].get_name(), m_new);
  matrices[mID] = mm;
}

/* Select fill mode */
void fill() {
  if (argc() != 2) {
    printf("One argument needed. Usage: 'fill <matrixname>'\n");
    return;
  }
  string name = cmdargs[1];
  UI mID;
  if ((mID = find_matrix(name)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found\n", name.c_str());
    return;
  }
  string answer;
  answer =
    ask_for_string("How do you want to fill? [random, hand, from, ident]");
  if (answer == "random") {
    fill_random(mID);
  } else if (answer == "hand") {
    fill_hand(mID);
  } else if (answer == "from") {
    fill_from(mID);
  } else if (answer == "ident") {
    fill_ident(mID);
  } else {
    fprintf(stderr, "Unknown fill mode.\n");
  }
}

/* Transpone matrix */
void transpone() {
  if (argc() != 2) {
    printf("One argument needed. Usage: 'trans <matrixname>'\n");
    return;
  }
  string name = cmdargs[1];
  UI mID;
  if ((mID = find_matrix(name)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found\n", name.c_str());
    return;
  }
  matrices[mID].transpone();
}

/* Prints info about given matrix */
void info() {
  if (argc() != 2) {
    printf("One argument needed. Usage: 'info <matrixname>'\n");
    return;
  }
  string name = cmdargs[1];
  UI mID;
  if ((mID = find_matrix(name)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found\n", name.c_str());
    return;
  }
  printf("Matrix \"%s\"\n", name.c_str());
  printf("  .. has %i rows\n", matrices[mID].get_rows());
  printf("  .. has %i cols\n", matrices[mID].get_cols());
  if (matrices[mID].is_square()) {
    double det = matrices[mID].get_determinant();
    printf("  .. is square.\n");
    printf("  .. has determinant: %.3f.\n", det);
  } else {
    printf("  .. is not square.\n");
  }
}

/* Adds two matrices and store result into third matrix */
void adding() {
  if (argc() != 4) {
    printf("Three args needed. Usage: '+ <matrix1> <matrix2> <matrix3>\n");
    return;
  }
  UI mID1, mID2;
  if ((mID1 = find_matrix(cmdargs[1])) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", cmdargs[1].c_str());
  } else if ((mID2 = find_matrix(cmdargs[2])) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", cmdargs[2].c_str());
  } else if (find_matrix(cmdargs[3]) != matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" already exists. ", cmdargs[3].c_str());
    fprintf(stderr, "Select other name of matrix to store result.\n");
  } else if ((matrices[mID1]).get_cols() != (matrices[mID2]).get_cols() ||
      (matrices[mID1]).get_rows() != (matrices[mID2]).get_rows() ) {
    fprintf(stderr, "Matrices must have same number of rows and cols.\n");
  } else {
    matrices.push_back(matrices[mID1].add(&matrices[mID2], &cmdargs[3]));
  }
}

/* Multiply two matrices */
void multiply() {
  if (argc() != 4) {
    printf("Three args needed. Usage: '* <matrix1> <matrix2> <matrix3>\n");
    return;
  }

  UI mID1, mID2;
  if ((mID1 = find_matrix(cmdargs[1])) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", cmdargs[1].c_str());
  } else if ((mID2 = find_matrix(cmdargs[2])) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", cmdargs[2].c_str());
  } else if (find_matrix(cmdargs[3]) != matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" already exists. ", cmdargs[3].c_str());
    fprintf(stderr, "Select other name of matrix to store result.\n");
  } else if ((matrices[mID1]).get_cols() != (matrices[mID2]).get_rows()) {
    fprintf(stderr, "Matrix \"%s\" must have same number ", cmdargs[1].c_str());
    fprintf(stderr, "of cols as number of lines in matrix ");
    fprintf(stderr, "\"%s\".\n", cmdargs[2].c_str());
  } else {
    matrices.push_back(matrices[mID1].multiply(&matrices[mID2], &cmdargs[3]));
  }
}

void multiply_c() {
  if (argc() != 2) {
    printf("One argument needed. Usage: 'mulc <matrixname>'\n");
    return;
  }
  string mname = cmdargs[1];
  UI mID;

  if ((mID = find_matrix(mname)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", mname.c_str());
    return;
  }

  if (matrices[mID].get_rows() == 0 || matrices[mID].get_cols() == 0) {
    fprintf(stderr, "Matrix \"%s\" is empty.", mname.c_str());
    return;
  }

  matrix_t m;
  int scalar = ask_for_number("Scalar");
  matrices[mID].multiply_c(&scalar);
}

void det() {
  if (argc() != 2) {
    printf("One argument needed. Usage: 'det <matrixname>'\n");
    return;
  }
  string name = cmdargs[1];
  UI mID;
  if ((mID = find_matrix(name)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", name.c_str());
  } else if (matrices[mID].is_empty()) {
    printf("Matrix \"%s\" is empty.\n", name.c_str());
  } else if (matrices[mID].is_square()) {
    double det = matrices[mID].get_determinant();
    printf("Matrix \"%s\" has determinant: %f\n", name.c_str(), det);
  } else {
    printf("Matrix \"%s\" is not square matrix. ", name.c_str());
    printf("Determinant cannot be calculated.\n");
  }
}

void load() {
  if (argc() != 3) {
    printf("One argument needed. Usage: 'load <matrixname> <filename>'\n");
    return;
  }
  string matrix_name = cmdargs[1];
  string file_name = cmdargs[2];
  UI mID;
  if ((mID = find_matrix(matrix_name)) != matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" already exists.\n", matrix_name.c_str());
  } else {
    matrix_t raw_matrix;
    int retval = load_matrix_from_file(&file_name, &raw_matrix);
    if (retval != 0) {
      printf("Error loading matrix from file \"%s\".\n", file_name.c_str());
    } else {
      Matrix loadedmatrix(matrix_name, raw_matrix);
      matrices.push_back(loadedmatrix);
      printf("Matrix \"%s\" loaded from file \"%s\".\n",
          matrix_name.c_str(), file_name.c_str());
    }
  }
}

void save() {
  if (argc() != 3) {
    printf("One argument needed. Usage: 'save <matrixname> <filename>'\n");
    return;
  }
  string matrix_name = cmdargs[1];
  string file_name = cmdargs[2];
  UI mID;
  if ((mID = find_matrix(matrix_name)) == matrices.size()) {
    fprintf(stderr, "Matrix \"%s\" not found.\n", matrix_name.c_str());
  } else if (matrices[mID].is_empty()) {
    printf("Matrix \"%s\" is empty.\n", matrix_name.c_str());
    printf("I will not store empty matrix.\n");
  } else {
    matrix_t raw_matrix = matrices[mID].get_matrix();
    UI rows = matrices[mID].get_rows();
    UI cols = matrices[mID].get_cols();
    int bytes_written =
      save_matrix_to_file(&file_name, &raw_matrix, &rows, &cols);
    if (bytes_written == -1) {
      printf("Error. Cannot save matrix \"%s\" to file \"%s\".\n",
          matrix_name.c_str(), file_name.c_str());
    } else {
      printf("Matrix \"%s\" has been written into file \"%s\" (%i bytes).\n",
          matrix_name.c_str(), file_name.c_str(), bytes_written);
    }
  }
}

/* Processing rutine
 * get a command as string and decides what to do
 */
bool process(const string & cmdline) {
  string command;
  cmdargs = explode(string(" "), cmdline);
  if (!cmdargs.size()) {
    fprintf(stderr, "Use command 'help' for list of availible commands.\n");
    return(true);
  } else {
    command = cmdargs[0];
  }

  if (command == "exit") {
    return(false);
  } else if (command == "quit") {
    return(false);
  } else if (command == "q") {
    return(false);
  } else if (command == "help") {
    help();
  } else if (command == "ls") {
    list();
  } else if (command == "list") {
    list();
  } else if (command == "print") {
    print();
  } else if (command == "p") {
    print();
  } else if (command == "new") {
    create();
  } else if (command == "cp") {
    copy();
  } else if (command == "ren") {
    rename();
  } else if (command == "mv") {
    rename();
  } else if (command == "rm") {
    remove();
  } else if (command == "fill") {
    fill();
  } else if (command == "tran") {
    transpone();
  } else if (command == "info") {
    info();
  } else if (command == "+") {
    adding();
  } else if (command == "*") {
    multiply();
  } else if (command == "add") {
    adding();
  } else if (command == "mul") {
    multiply();
  } else if (command == "mulc") {
    multiply_c();
  } else if (command == "det") {
    det();
  } else if (command == "load") {
    load();
  } else if (command == "save") {
    save();
  } else {
    fprintf(stderr, "Unknown command: '%s'.", command.c_str());
    fprintf(stderr, " Use command 'help' for list of availible commands.\n");
  }
  return(true);
}

/* Main rutine
 * prints welcome message and catches user input
 */
int main(const int argc, char **argv) {
  const string prgname = string(*argv);
  bool print_welcome = true;
  switch (argc) {
    case 1:
      break;
    case 2:
      argv++;
      if (string(*argv) == "-q" || string(*argv) == "--quiet") {
        print_welcome = false;
        break;
      }
    default:
      printf("Usage: %s [-q|--quiet]\n", prgname.c_str());
      return(1);
  }

  if (print_welcome) {
    printf("smcalc 1.0\n");
    printf("For informations go to http://smcalc.sourceforge.net\n");
    printf("For help type 'help' and press enter.\n");
    printf("\n");
  }

  srand((unsigned) time(NULL));

  char   cmd[kCmdSize];
  bool   run;  // signalize end of program
  int    i;
  string cmd_str;

  Matrix demo = create_demo_matrix();
  matrices.push_back(demo);

  run = true;
  do {
    i = 0;
    memset(cmd, 0, kCmdSize);
    cmd_str.clear();
    printf(">>> ");
    fflush(stdout);
    fgets(cmd, kCmdSize, stdin);
    while (*(cmd + i) != '\n' && *(cmd + i) != '\0') {
      cmd_str.push_back(*(cmd+i));
      i++;
    }
    run = process(cmd_str);
  } while (run);
  return(0);
}

/* EOF */
