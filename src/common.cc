/* Common functions for smcalc project

Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <cstdlib>
#include <ctime>
#include "./smcalc.h"
#include "./common.h"

using std::vector;
using std::string;

int rnd(int min, int max) {
  if (max < min) {
    int tmp = max;
    max = min;
    min = tmp;
  }
  return ((rand() % (max - min + 1)) + min);
}

/* Explode text by given patten. Doesn't work 100% */
vector<string> explode(const string &pattern, const string &text) {
  vector<string> tokens;
  int mark = 0;
  size_t found;
  string token;

  if ( pattern.empty() ) {
    tokens.push_back(text);
    return tokens;
  } else if (text.empty()) {
    return tokens;
  }

  while (1) {
    found = text.find(pattern, (size_t) mark);
    if (found != string::npos) {
      if (static_cast<int>(found) - mark) {
        token = text.substr(mark, static_cast<int>(found) - mark);
        tokens.push_back(token);
      }
      mark = static_cast<int>(found) + static_cast<int>(pattern.length());
    } else {
      if (mark - text.length()) {
        token = text.substr(mark, text.length());
        tokens.push_back(token);
      }
      break;
    }
  }

  return tokens;
}

/* EOF */
