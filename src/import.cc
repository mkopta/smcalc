/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <cstdio>
#include <string>
#include "./smcalc.h"
#include "./import.h"

using std::string;
using std::vector;

int load_matrix_from_file(const string *file_name, matrix_t *matrix) {
  FILE *input;
  int rows = 0;
  int cols = 0;
  int x;

  input = fopen(file_name->c_str(), "r");
  if (!input) return(1);
  fscanf(input, "%i", &rows);
  fscanf(input, "%i", &cols);
  if ((rows < 1) || (cols < 1)) return(1);
  matrix->clear();
  for (int i = 0; i < rows; i++) {
    vector<int> line;
    for (int j = 0; j < cols; j++) {
      x = 0;
      if (fscanf(input, "%i", &x) != EOF) {
        line.push_back(x);
      } else {
        return(1);
      }
    }
    matrix->push_back(line);
  }
  return(0);
}

/* EOF */
