/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#ifndef _smcalc_h_42567463782983920_
#define _smcalc_h_42567463782983920_

#include <vector>
#include <cstdio>

#ifdef DEBUG
#define DEBUG 1
#define PRINT_DEBUG(s) fprintf(stderr, "%s [%i]: %s\n", __FILE__, __LINE__, s);
#else
#define DEBUG 0
#define PRINT_DEBUG(s)
#endif

typedef unsigned int UI;
typedef std::vector< std::vector<int> > matrix_t;
typedef std::vector< std::vector<double> > dmatrix_t;

#endif /* _smcalc_h_42567463782983920_ */

/* EOF */
