/* Copyright (c) 2009 <Martin Kopta, martin@kopta.eu>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
****************************************************************/

#include <sstream>
#include <fstream>
#include "./export.h"

using std::string;
using std::vector;
using std::stringstream;
using std::endl;
using std::ofstream;

int save_matrix_to_file(const string *file_name, const matrix_t *raw_matrix,
    const UI *rows, const UI *cols) {
  stringstream output;
  int bytes = 0;
  output << *rows << " " << *cols << endl;
  output << endl;
  for (UI i = 0; i < *rows; i++) {
    for (UI j = 0; j < *cols; j++) {
      output << (*raw_matrix)[i][j] << " ";
    }
    output << endl;
  }
  ofstream fs(file_name->c_str());
  if (fs.is_open()) {
    fs << output.str();
    fs.close();
    bytes = output.str().size();
  } else {
    bytes = -1;
  }
  return(bytes);
}


/* EOF */
